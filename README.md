# BrwR Calc

# Introduction

This is a project that I created while learning JavaScript and React.

BrwR Calc is a simple calculator/conversion app for my most used brewing conversions. Instead of having an app with more than I need, I created a simple app with only the calculators that I need.

# Modules (Functionality)

Each calculator/converter in the app is considered a _module_. Each module is listed below with its intended usage.

## Brix Converter

Converting between Brix and Specific Gravity is the conversion that I use the most during a brew day.

You can type the Brix value in to get a Specific Gravity or you can type in an SG value to get Brix. The converstion works both ways.

## Kettle Volume

I use a metal measuring rule to find the amount of water/wort in my brew kettle. By using the known radius of my kettle, I can enter the height of the liquid and the app will calculate the volume in gallons.

Because my metal rule is in inches and has fractions, factional values can be entered.

Example height inputs:

-   5
-   5.5
-   5 1/2
-   5 1/8

Entering your target volume will also give you the height of liquid you need.

### Changing Kettle Radius

The default kettle radius matches my own kettle. If you need to change it for your own usage, it can be changed by clicking the gear icon and changing the radius in the options menu.

## Temperature Converter

Converts from Fahrenheit to Celcius and back. Straight forward.

# Links and Such

The official repository can be found at:
https://bitbucket.org/tmorgan001/brwr-calc/

To see the code in action:
https://dev.tmorgan.cloud/brwr/

For other projects that I'm working on while I learn JavaScript and React:
https://dev.tmorgan.cloud/
