import React from 'react'
import arrowDown from '../img/arrow-down.svg'
import arrowUp from '../img/arrow-up.svg'

class ModuleHeader extends React.Component {
    handleClick = () => {
        this.props.handleHeaderClick(this.props.moduleName)
    }

    render() {
        return (
            <button
                className="Module-Header rounded boarder-bottom boarder-right"
                onClick={this.handleClick}
            >
                <h2>{this.props.moduleTitle}</h2>
                <img
                    className="arrow"
                    alt=""
                    src={this.props.arrowOpen ? arrowUp : arrowDown}
                />
            </button>
        )
    }
}

export default ModuleHeader
