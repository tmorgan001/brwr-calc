import React from 'react'
import '../App.css'
import Header from './Header.js'
import ModuleContainer from './ModuleContainer.js'
import OptionModal from './OptionModal.js'

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appName: 'BrwR Calc',
            optKettleRadius: 6.72,
            showOptionsMenu: false
        }
    }

    handleSettingsClick = () => {
        if (this.state.showOptionsMenu === false) {
            this.setState({ showOptionsMenu: true })
        }
    }

    handleSettingsSave = newValue => {
        this.setState({ optKettleRadius: Number(newValue) })
        this.setState({ showOptionsMenu: false })
    }

    handleSettingsCancel = () => {
        this.setState({ showOptionsMenu: false })
    }

    render() {
        return (
            <div id="App">
                <Header
                    appName={this.state.appName}
                    handleSettingsClick={this.handleSettingsClick}
                />
                <ModuleContainer optKettleRadius={this.state.optKettleRadius} />
                <OptionModal
                    showOptionsMenu={this.state.showOptionsMenu}
                    handleSettingsSave={this.handleSettingsSave}
                    handleSettingsCancel={this.handleSettingsCancel}
                    optKettleRadius={this.state.optKettleRadius}
                />
            </div>
        )
    }
}

export default App
