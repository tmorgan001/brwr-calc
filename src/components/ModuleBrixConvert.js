import React from 'react'
import ModuleHeader from './ModuleHeader.js'
import ModuleBody from './ModuleBody.js'

class ModuleBrixConvert extends React.Component {
    constructor(props) {
        super(props)
        this.name = 'brixConvert'
        this.moduleTitle = 'Brix Converter'
        this.state = {
            sg: '. . .',
            brix: '. . .'
        }
    }

    // Converts Brix to SG
    handleBrixInput = event => {
        const brixVal = event.target.value
        if (!isNaN(brixVal) && brixVal !== '') {
            let sgVal = (
                brixVal / (258.6 - (brixVal / 258.2) * 227.1) +
                1
            ).toFixed(3)

            this.setState({ sg: sgVal, brix: brixVal })
        } else {
            this.setState({ sg: '', brix: '' })
        }
    }

    // Converts SG to Brix
    handleSGInput = event => {
        const sgVal = event.target.value
        if (!isNaN(sgVal) && sgVal !== '' && (sgVal >= 1 && sgVal <= 1.2)) {
            let brixVal = (
                ((182.4601 * sgVal - 775.6821) * sgVal + 1262.7794) * sgVal -
                669.5622
            ).toFixed(2)

            this.setState({ sg: sgVal, brix: brixVal })
        } else {
            this.setState({ sg: '', brix: '' })
        }
    }

    moduleBodyTemplate = () => (
        <div>
            <form>
                <div className="form-group">
                    <label htmlFor="brixInput">Brix</label>
                    <input
                        className="form-control"
                        id="brixInput"
                        type="number"
                        placeholder=". . ."
                        onChange={event => this.handleBrixInput(event)}
                        value={this.state.brix}
                    />
                </div>
                <div>
                    <label htmlFor="sgOutput">Specific Gravity</label>
                    <input
                        className="form-control"
                        id="sgOutput"
                        type="number"
                        placeholder=". . ."
                        onChange={event => this.handleSGInput(event)}
                        value={this.state.sg}
                    />
                </div>
            </form>
        </div>
    )

    render() {
        return (
            <div className="App-Module">
                <ModuleHeader
                    moduleName={this.name}
                    handleHeaderClick={this.props.handleHeaderClick}
                    arrowOpen={this.props.show === this.name}
                    moduleTitle={this.moduleTitle}
                />
                <ModuleBody
                    moduleName={this.name}
                    show={this.props.show !== this.name && 'hide'}
                    moduleBodyTemplate={this.moduleBodyTemplate()}
                />
            </div>
        )
    }
}

export default ModuleBrixConvert
