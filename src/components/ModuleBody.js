import React from 'react'

const ModuleBody = props => (
    <div className={'Module-Body ' + props.show}>
        {props.moduleBodyTemplate}
    </div>
)

export default ModuleBody
