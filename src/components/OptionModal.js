import React from 'react'
import Modal from 'react-modal'

class OptionModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            optKettleRadius: props.optKettleRadius
        }
    }

    handleOptKettleRadiusChange = event => {
        const optKettleRadius = event.target.value
        this.setState({ optKettleRadius: optKettleRadius })
    }

    handleSettingsSave = () => {
        this.props.handleSettingsSave(this.state.optKettleRadius)
    }

    render() {
        return (
            <Modal
                isOpen={this.props.showOptionsMenu}
                contentLabel="Options"
                onRequestClose={this.props.handleSettingsCancel}
                closeTimeoutMS={200}
                className="optionsMenu"
                ariaHideApp={false}
            >
                <div className="Module-Body">
                    <h3>About</h3>
                    <p>
                        BrwR Calc
                        <br />
                        2019.05.26
                        <br />
                        Created by Tim Morgan
                        <br />
                    </p>
                    <h3>Options</h3>

                    <form>
                        <div className="form-group">
                            <label htmlFor="radiusInput">Kettle Radius</label>
                            <input
                                className="form-control"
                                id="radiusInput"
                                type="number"
                                placeholder="inches"
                                onChange={event =>
                                    this.handleOptKettleRadiusChange(event)
                                }
                                value={this.state.optKettleRadius}
                            />
                        </div>
                    </form>

                    <button
                        className="moduleBtn Module-Header rounded boarder-bottom boarder-right"
                        onClick={this.props.handleSettingsCancel}
                    >
                        Cancel
                    </button>
                    <br />
                    <button
                        className="moduleBtn Module-Header rounded boarder-bottom boarder-right"
                        onClick={this.handleSettingsSave}
                    >
                        Save
                    </button>
                </div>
            </Modal>
        )
    }
}

export default OptionModal
