import React from 'react'
import ModuleHeader from './ModuleHeader.js'
import ModuleBody from './ModuleBody.js'

class ModuleTempConvert extends React.Component {
    constructor(props) {
        super(props)
        this.name = 'tempConvert'
        this.moduleTitle = 'Temperature Converter'
        this.state = {
            tempC: '',
            tempF: ''
        }
    }

    handleTempInput = e => {
        const val = e.target.value
        if (e.target.id === 'tempF' && !isNaN(val) && val !== '') {
            let cVal = ((val - 32) * (5 / 9)).toFixed(1)
            this.setState({ tempC: cVal, tempF: val })
        } else if (e.target.id === 'tempC' && !isNaN(val) && val !== '') {
            let fVal = (val * (9 / 5) + 32).toFixed(1)
            this.setState({ tempF: fVal, tempC: val })
        } else {
            this.setState({ tempC: '', tempF: '' })
        }
    }

    moduleBodyTemplate = () => (
        <div>
            <form>
                <div className="form-group">
                    <label htmlFor="tempF">Fahrenheit</label>
                    <input
                        className="form-control"
                        id="tempF"
                        type="number"
                        placeholder=". . ."
                        onChange={event => this.handleTempInput(event)}
                        value={this.state.tempF}
                    />
                </div>
                <div>
                    <label htmlFor="tempC">Celsius</label>
                    <input
                        className="form-control"
                        id="tempC"
                        type="number"
                        placeholder=". . ."
                        onChange={event => this.handleTempInput(event)}
                        value={this.state.tempC}
                    />
                </div>
            </form>
        </div>
    )

    render() {
        return (
            <div className="App-Module">
                <ModuleHeader
                    moduleName={this.name}
                    handleHeaderClick={this.props.handleHeaderClick}
                    arrowOpen={this.props.show === this.name}
                    moduleTitle={this.moduleTitle}
                />
                <ModuleBody
                    moduleName={this.name}
                    show={this.props.show !== this.name && 'hide'}
                    moduleBodyTemplate={this.moduleBodyTemplate()}
                />
            </div>
        )
    }
}

export default ModuleTempConvert
