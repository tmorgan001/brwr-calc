import React from 'react'
import settingsBtn from '../img/black-settings-button.svg'

const Header = props => (
    <div id="App-header">
        <h1>
            {props.appName}
            <img
                className="settingsBtn"
                alt=""
                src={settingsBtn}
                onClick={props.handleSettingsClick}
            />
        </h1>
    </div>
)

export default Header
