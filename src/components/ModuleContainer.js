import React from 'react'
import ModuleBrixConvert from './ModuleBrixConvert.js'
import ModuleKettleVol from './ModuleKettleVol.js'
import ModuleTempConvert from './ModuleTempConvert.js'

class ModuleContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: ''
        }
    }

    handleHeaderClick = moduleName => {
        // toggle the module on header click
        if (this.state.show === moduleName) {
            moduleName = ''
        }
        this.setState({ show: moduleName })
    }

    render() {
        return (
            <div className="container">
                <div className="row align-items-center justify-content-center">
                    <div id="Main-App" className="col">
                        <ModuleBrixConvert
                            show={this.state.show}
                            handleHeaderClick={this.handleHeaderClick}
                        />
                        <ModuleKettleVol
                            show={this.state.show}
                            handleHeaderClick={this.handleHeaderClick}
                            optKettleRadius={this.props.optKettleRadius}
                        />
                        <ModuleTempConvert
                            show={this.state.show}
                            handleHeaderClick={this.handleHeaderClick}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default ModuleContainer
