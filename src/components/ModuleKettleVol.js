import React from 'react'
import ModuleHeader from './ModuleHeader.js'
import ModuleBody from './ModuleBody.js'

class ModuleKettleVol extends React.Component {
    constructor(props) {
        super(props)
        this.name = 'kettleVol'
        this.moduleTitle = 'Kettle Volume'
        this.state = {
            height: '',
            volume: '',
            radius: props.optKettleRadius
        }
    }

    handleHeightInput = event => {
        // Since adding an option menu, I have to check for an updated radius
        this.setState({ radius: this.props.optKettleRadius })
        const hVal = event.target.value
        /*
            I use RegEx's to determine the format of user input.
            Input format examples:
                28 => reWholeNumber
                28.5 => reDecimalNumber
                28 1/4 => reFractionNumber
            Whole and Decimal numbers are calculated as-is.
        */
        const reWholeNumber = /^\d+$/
        const reDecimalNumber = /^\d+\.\d+$/
        const reFractionNumber = /^\d+\s\d+\/\d+$/
        if (reWholeNumber.exec(hVal) || reDecimalNumber.exec(hVal)) {
            /* 
                volume = pi * r^2 * h
            */
            const vVal = (
                Math.PI *
                Math.pow(this.state.radius, 2) *
                hVal
            ).toFixed(2)
            /* 
                Converting cubic inches to gallons.
                1 cu inch = 0.004329 gallons
            */
            const gallons = (vVal * 0.004329).toFixed(2)
            this.setState({ height: hVal, volume: gallons })
        } else if (reFractionNumber.exec(hVal)) {
            /*
                Input valute contains a fraction.
                First step is to split the input into three values:
                0-whole number, 1-numerator(top), 2-denominator(bottom).
                The split(/[ /]/) splits the input by whitespace OR '/'.
                We divide the numerator by the denominator to get a decimal.
                Then add the decimal to the whole number.
            */
            const splitValues = hVal.split(/[ /]/)
            const fractionAsDecimal =
                Number(splitValues[1]) / Number(splitValues[2])
            const heightValue = Number(splitValues[0]) + fractionAsDecimal
            /*
                Perform the actual formula conversion here.
            */
            const vVal = (
                Math.PI *
                Math.pow(this.state.radius, 2) *
                heightValue
            ).toFixed(2)
            const gallons = (vVal * 0.004329).toFixed(2)
            this.setState({ height: hVal, volume: gallons })
        } else {
            /* 
                No match. Not a calculable input value.
            */
            this.setState({ height: hVal, volume: '' })
            return
        }
    }

    handleVolumeInput = event => {
        // Since adding an option menu, I have to check for an updated radius
        this.setState({ radius: this.props.optKettleRadius })
        let vVal = event.target.value
        if (!isNaN(vVal) && vVal !== '') {
            let hVal = (
                vVal /
                (0.004329 * Math.PI * Math.pow(this.state.radius, 2))
            ).toFixed(2)
            this.setState({ volume: vVal, height: hVal })
        } else {
            this.setState({ volume: '', height: '' })
        }
    }

    moduleBodyTemplate = () => (
        <form>
            <div className="form-group">
                <label htmlFor="heightInput">Height</label>
                <input
                    className="form-control"
                    id="heightInput"
                    type="text"
                    placeholder="inches"
                    onChange={event => this.handleHeightInput(event)}
                    value={this.state.height}
                />

                <label htmlFor="volumeInput">Volume</label>
                <input
                    className="form-control"
                    id="volumeInput"
                    type="number"
                    placeholder="gallons"
                    onChange={event => this.handleVolumeInput(event)}
                    value={this.state.volume}
                />
            </div>
        </form>
    )

    render() {
        return (
            <div className="App-Module">
                <ModuleHeader
                    moduleName={this.name}
                    handleHeaderClick={this.props.handleHeaderClick}
                    arrowOpen={this.props.show === this.name}
                    moduleTitle={this.moduleTitle}
                />
                <ModuleBody
                    moduleName={this.name}
                    show={this.props.show !== this.name && 'hide'}
                    moduleBodyTemplate={this.moduleBodyTemplate()}
                />
            </div>
        )
    }
}

export default ModuleKettleVol
